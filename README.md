# Pagerank Algorithm

## Repository

This repository provides a serial implementation of the pagerank algorithm in C language, as well as a parallel equivalent using OpenMP. The project was undertaken as part of the "Parallel and distributed systems" course of AUTH university.

The code was tested for different data sets and information regarding the execution time and correctness were extracted.

## Dependencies

Only a compiler is needed (e.g. gcc).

## Compilation

To compile just run:

```sh
$ make
```

## Usage

Run the code with the command:
```sh
$ ./pagerank [-c convergence_criterion] [-m max_iterations] [-a alpha] [-v] [-h] [-o output_filename] <graph_file>
```
where:

 1. **convergence_criterion** is the convergence tolerance criterion
 2. **max_iterations** is the maximum number of iterations to perform
 3. **alpha** is the damping factor
 4. **-v** enables verbal output
 5. **-h** enables history output to file
 6. **output_filename** is the output filename and path
 7. **graph_file** is the dataset filename and path

The parallel version also provides the option -t number_of_threads where **number_of_threads** is the number of threads to use.
